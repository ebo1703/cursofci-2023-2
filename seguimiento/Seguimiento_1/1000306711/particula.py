from scipy.integrate import solve_ivp
import numpy as np
import matplotlib.pyplot as plt

class ArgumentosInvalidos(Exception):
    def __init__(self, mensaje):
        self.mensaje = mensaje
        super().__init__(self.mensaje)

class ParticulaCampo(object):
    def __init__(self,m,q,B,Ek,theta,phi,x0,y0,z0,tf):
      self.m=m
      self.q=q
      self.B=B
      self.Ek=Ek
      self.theta=theta
      self.phi=phi
      self.x0=x0
      self.y0=y0
      self.z0=z0
      self.V=np.sqrt(2*self.Ek/self.m)
      self.vx0=self.V*np.sin(self.theta)*np.cos(self.phi)
      self.vy0=self.V*np.sin(self.theta)*np.sin(self.phi)
      self.vz0=self.V*np.cos(self.theta)
      self.ti=0
      self.tf=tf
      self.t=[]
      self.x=[]
      self.y=[]
      self.z=[]
      self.vx=[]
      self.vy=[]
      if  self.phi==np.pi/2:
        self.x0c=x0
      else:
        self.x0c=self.x0-(self.y0/self.vy0)*self.vx0

      self.z0c=self.z0-(self.y0/self.vy0)*self.vz0
      self.escapa=False
      self.paso=0
      self.validar_argumentos()

    def validar_argumentos(self):
        if self.phi <= 0 or self.phi >= np.pi:
            raise ArgumentosInvalidos("Argumentos absurdos")
        if self.theta <= 0 or self.theta >= np.pi:
            raise ArgumentosInvalidos("Argumentos absurdos")                          

    def intervalo(self):
     self.t=np.linspace(self.ti,self.tf,(self.tf-self.ti)*1000) 
     return self.t

    def ecuaciones_mov(self):
     self.intervalo()

     def sistema_ecuaciones(t,y):
      dydt=np.zeros(4)
      x,x_prime,y,y_prime=y
      dydt[0]=x_prime
      dydt[1]=((self.q*self.B)/self.m)*y_prime
      dydt[2]=y_prime
      dydt[3]=-((self.q*self.B)/self.m)*x_prime
      return dydt

     sol=solve_ivp(sistema_ecuaciones,[self.ti,self.tf],[self.x0c,self.vx0,0,self.vy0],t_eval=self.t)
     self.x=sol.y[0]
     self.y=sol.y[2]
     self.vx=sol.y[1]
     self.vy=sol.y[3]
     return self.x,self.y,self.vx,self.vy

    def pos_z(self):
      self.intervalo()
      if self.theta==np.pi/2:
        self.z=np.full((len(self.t)),self.z0)
        self.z0c=self.z0
      else:
        self.z=[self.vz0*self.t[i]+self.z0c for i in range(len(self.t))] 
      return self.z
    
    def figplot(self,name="Gráfica"):
      plt.style.use("dark_background")
      x,y,vx,vy=self.ecuaciones_mov()
      self.pos_z()
      xe=[]
      ye=[]
      ze=[]
      vvx=[]
      vvy=[]
      for i in range(len(y)):
        if y[i] < 0:
          self.escapa=True
          for j in range(i+1):
            xe.append(x[j])
            ye.append(y[j])
            ze.append(self.z[j])
            vvx.append(vx[j])
            vvy.append(vy[j])
          break  

      if self.escapa==True:      
          fig=plt.figure(figsize=(7,5))
          ax=fig.add_subplot(111,projection='3d')
          ax.plot([self.x0,self.x0c],[self.y0,0],[self.z0,self.z0c],color="blue")
          ax.plot([xe[-1],xe[-1]+(vvx[-1]*(self.y0))/vvy[-1]],[0,self.y0],[ze[-1],ze[-1]+(self.vz0*(self.y0))/vvy[-1]],color="blue")
          ax.plot(xe,ye,ze,color="blue",label="Trayectoria de la partícula")
          ax.set_title("Partícula cargada entrando a un campo magnético uniforme",fontsize=15)
          ax.set_xlabel("x")
          ax.set_ylabel("y")
          ax.set_zlabel("z")
          ax.view_init(elev=30,azim=30)
          ax.legend()
          plt.savefig(name)
          plt.show()  
         
      else:
          fig=plt.figure(figsize=(7,5))
          ax=fig.add_subplot(111,projection='3d')
          ax.plot([self.x0,self.x0c],[self.y0,0],[self.z0,self.z0c],color="blue")
          ax.plot(x,y,self.z,color="blue",label="Trayectoria de la partícula")
          ax.set_title("Partícula cargada entrando a un campo magnético uniforme",fontsize=15)
          ax.set_xlabel("x")
          ax.set_ylabel("y")
          ax.set_zlabel("z")
          ax.view_init(elev=30,azim=45)
          ax.legend()
          plt.savefig(name)
          plt.show()